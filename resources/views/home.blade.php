@extends('layouts.app')

@section('content')
    <transaction-component :transactions='{!! json_encode($transactions) !!}'></transaction-component><hr>
@endsection
