<?php

namespace App\Http\Controllers\API;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TransactionController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'amount' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 422);
        }

        $transaction = Transaction::create($request->only(['customer_id', 'amount']))->makeHidden(['updated_at']);

        return response()->json($transaction, 201);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 422);
        }

        $transaction = Transaction::find($id);

        if(is_null($transaction)){
            return response()->json([], 404);
        }

        $transaction->update($request->only(['amount']));

        return response()->json($transaction->makeHidden(['updated_at']), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        if(is_null($transaction)){
            return response()->json(['status' => 'fail'], 404);
        }

        $transaction->delete();

        return response()->json(['status' => 'success'], 204);
    }

    /**
     * @param $customer_id
     * @param $transaction_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransaction($customer_id, $transaction_id)
    {
        $transaction = Transaction::where('id', $transaction_id)->where('customer_id', $customer_id)->first();

        if(is_null($transaction)){
            return response()->json([], 404);
        }

        return response()->json($transaction->makeHidden(['customer_id', 'updated_at']), 200);
    }


}
