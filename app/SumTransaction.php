<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SumTransaction extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'sum', 'from_date', 'to_date'
    ];
}
