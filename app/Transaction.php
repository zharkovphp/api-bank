<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;

class Transaction extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'customer_id', 'amount'
    ];


    /**
     * @var array
     */
    public $sortable = [
        'amount',
        'created_at',
        'customer_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'id', 'customer_id');
    }

}
